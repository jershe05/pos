<?php
namespace App\Contracts\Model;

Interface DatabaseContract
{
    public function addNew(array $aData) : bool;
    public function edit() : bool;
    public function delete() : bool;
    public function getAll() : array;
    public function getStatus() : bool;
}
