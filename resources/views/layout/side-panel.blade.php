<!-- Sidebar - Brand -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-users-cog"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Admin Page</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="index.html">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->

    <!-- Nav Item - Users Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-users"></i>
            <span>Users</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">User Settings</h6>
                <span class="collapse-item clickable"><i class="fas fa-plus-circle"></i> Add New</span>
                <a class="collapse-item" href="#"><i class="fas fa-list"></i> User List</a>
            </div>
        </div>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Settings
    </div>

    <!-- Nav Item - Products Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProductMenu" aria-expanded="true" aria-controls="collapseProductMenu">
            <i class="fas fa-fw fa-cubes"></i>
            <span>Products</span>
        </a>
        <div id="collapseProductMenu" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Product Settings</h6>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalAddProduct">
                    <i class="fas fa-plus-circle"></i> Add Product
                </span>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalAddProduct">
                    <i class="fas fa-list"></i> Product List
                </span>
            </div>
        </div>
    </li>
    <!-- Nav Item - Category Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategoryMenu" aria-expanded="true" aria-controls="collapseCategoryMenu">
            <i class="fas fa-fw fa-window-restore"></i>
            <span>Category</span>
        </a>
        <div id="collapseCategoryMenu" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Category Settings</h6>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalAddCategory">
                    <i class="fas fa-plus-circle"></i> Add Category
                </span>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalCategoryList">
                    <i class="fas fa-list"></i> Category List
                </span>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDesignMenu" aria-expanded="true" aria-controls="collapseDesignMenu">
            <i class="fas fa-fw fa-splotch"></i>
            <span>Product Designs</span>
        </a>
        <div id="collapseDesignMenu" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Design Settings</h6>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalAddProduct">
                    <i class="fas fa-plus-circle"></i> Add Design
                </span>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalAddProduct">
                    <i class="fas fa-list"></i> Design List
                </span>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Inventory
    </div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#stockItemsMenu" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Stock Items</span>
        </a>
        <div id="stockItemsMenu" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">STOCK SETTINGS:</h6>
                <span class="collapse-item clickable" data-toggle="modal" data-target="#modalAddStock">
                    <i class="fas fa-plus-circle"></i> Add Stock
                </span>
                <a class="collapse-item" href="register.html">
                    <i class="fas fa-list"></i> Stock List
                </a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
            <i class="fas fa-fw fa-folder"></i>
            <span>Pages</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Login Screens:</h6>
                <a class="collapse-item" href="login.html">Login</a>
                <a class="collapse-item" href="register.html">Register</a>
                <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
                <div class="collapse-divider"></div>
                <h6 class="collapse-header">Other Pages:</h6>
                <a class="collapse-item" href="404.html">404 Page</a>
                <a class="collapse-item" href="blank.html">Blank Page</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Charts</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="tables.html">
            <i class="fas fa-fw fa-table"></i>
            <span>Tables</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
