<?php

namespace App\Providers;

use App\Constants\AppStrings;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;


class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

       // $this->mapWebRoutes();
        $this->setUserRegistrationRoute();
        $this->setAccountRoute();
        $this->setCategoryRoute();
        //
    }

    public function setUserRegistrationRoute()
    {
        $aUserRoutes = AppStrings::USER_ROUTES;
        Route::middleware('web')
            ->namespace($aUserRoutes['namespace'])
            ->group(base_path($aUserRoutes['path']));
    }

    public function setAccountRoute()
    {
        $aAccountRoutes = AppStrings::AUTHENTICATE;
        Route::middleware('web')
            ->namespace($aAccountRoutes['namespace'])
            ->group(base_path($aAccountRoutes['path']));
    }

    public function setCategoryRoute()
    {
        $aCategoryRoutes = AppStrings::CATEGORY_ROUTES;
        Route::middleware('web')
            ->namespace($aCategoryRoutes['namespace'])
            ->group(base_path($aCategoryRoutes['path']));
    }


    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }
}
