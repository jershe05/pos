<?php
namespace App\Contracts\Bl\Validation;

Interface ValidationContract
{
    public function registerFields(array $oFields) : ValidationContract;
    public function loopThroughObjectsAndExecuteValidation(array $aFields, array $aFieldsToBeMatched) : ValidationContract;
    public function setValidationResult(array $aErrors, string $sField);
    public function getFieldsToBeComparedMatched($sField, $mValue);
    public function compareMatchingFields();
    public function filterResult() : ValidationContract;
    public function removeArrayIfEmpty() : ValidationContract;
    public function getResult() : array;
}
