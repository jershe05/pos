<?php

namespace App\Bl\Validation\ValidationObjects;

use App\Contracts\Bl\FormatTextContract;
use Illuminate\Support\Str;

class TitleFormatField extends AbstractInputField
{
    public function formatText()
    {
         $this->sValue = Str::title($this->sValue);
         return $this;
    }
}
