<?php
namespace App\Model;

use App\Constants\AppStrings;
use App\Contracts\Model\AccountContract;
use App\Contracts\Model\DatabaseContract;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;

class User extends Authenticatable implements DatabaseContract, AccountContract
{
    use Notifiable;

    protected $fillable = [
        'fname',
        'lname',
        'email',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $table = AppStrings::USER_TABLE;
    public function getAll(): array
    {
        $oResult = Cache::remember(AppStrings::USER_CACHE, 2, function() {
            return $this::all();
        });
    }

    public function addNew(array $aData) : bool {
        foreach ($aData as $sKey => $mValue) {
            $this->$sKey = $mValue;
        }
        return $this->save();
    }

    public function getStatus(): bool
    {

    }

    public function edit(): bool
    {

    }

    public function delete(): bool
    {

    }

    public function getRoles(int $iId) : int
    {
        return (int) $this::find($iId)->role;
    }

}
