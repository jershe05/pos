<?php
namespace App\Contracts\Bl;

Interface RegistrationContract
{
    public function register(array $aRegistrationDetails): array;
}
