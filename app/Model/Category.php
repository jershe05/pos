<?php
namespace App\Model;

use App\Constants\AppStrings;
use App\Contracts\Model\DatabaseContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Category extends Model implements DatabaseContract
{
    public $table = AppStrings::CATEGORY_TABLE;
    public function getAll(): array
    {
        $oResult = Cache::remember(AppStrings::USER_CACHE, 2, function() {
            return $this::all();
        });
    }

    public function addNew(array $aData) : bool {
        foreach ($aData as $sKey => $mValue) {
            $this->$sKey = $mValue;
        }
        return $this->save();
    }

    public function getStatus(): bool
    {

    }

    public function edit(): bool
    {

    }

    public function delete(): bool
    {

    }


}
