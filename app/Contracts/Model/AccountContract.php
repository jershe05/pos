<?php
namespace App\Contracts\Model;

Interface AccountContract
{
    public function getRoles(int $iId) : int;
}
