<?php

namespace App\Controllers\Auth;

use App\Bl\Authentication\UserAuthentication;
use App\Bl\Validation\ValidationObjects\DefaultField;
use App\Bl\Validation\ValidationObjects\PasswordFormatField;
use App\Bl\Validation\ValidationObjects\TitleFormatField;
use App\Constants\AppStrings;
use App\Contracts\Bl\Validation\ValidationContract;
use App\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    private $oBlValidation;
    public function __construct(ValidationContract $oValidation)
    {
        $this->oBlValidation = $oValidation;
    }

    public function instantiateObjectFields(): array
    {
        return [
            AppStrings::EMAIL_FIELD => new DefaultField(AppStrings::EMAIL_LOGIN_RULES),
            AppStrings::PASSWORD_FIELD => new PasswordFormatField(AppStrings::PASSWORD_RULES)
        ];
    }

    public function authenticate(Request $oRequest)
    {

        $aRequestParams = $oRequest->all()['params'];
        $oFields = $this->instantiateObjectFields();

        $aLoginCredentials = [
            'email'    => $aRequestParams['email'],
            'password' => $aRequestParams['password']
        ];

        $aResultAndMessages = $this->oBlValidation
            ->registerFields($oFields)
            ->loopThroughObjectsAndExecuteValidation($aLoginCredentials, [])
            ->filterResult()
            ->getResult();

        if ($aResultAndMessages[AppStrings::B_RESULT] === false) {
            return $aResultAndMessages;
        }

        $oLogin = new UserAuthentication($aLoginCredentials);
        return $oLogin->authenticate();
    }
}
