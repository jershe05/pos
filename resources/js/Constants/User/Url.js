const URL = {
    SAVE_REGISTRATION : '/registration/save',
    LOGIN_PAGE        : '/'
};

export default URL;
