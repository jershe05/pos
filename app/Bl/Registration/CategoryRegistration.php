<?php

namespace App\Bl\Registration;

use App\Bl\Validation\ValidationObjects\Email;
use App\Contracts\Bl\RegistrationContract;
use App\Contracts\Model\DatabaseContract;

class CategoryRegistration implements RegistrationContract
{
    private $oModel;
    public function __construct(DatabaseContract $oModel)
    {
        $this->oModel = $oModel;
    }
    public function register(array $aRegistrationDetails): array
    {
        return ['bResult' => $this->oModel->addNew($aRegistrationDetails)];
    }

}
