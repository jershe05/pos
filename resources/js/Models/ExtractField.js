import GLOBAL_STRINGS from "../Constants/Global/GlobalStrings";

class ExtractField
{
    constructor(oFields)
    {
        this.oFields = oFields;
    }

    exec()
    {
        let oField = this.oFields;
        $.each(oField, function(sKey, oObject){
            oField[sKey] = oObject[GLOBAL_STRINGS.CHILD_INDEX].getElement().val();
        });
        return oField;
    }
}
export default ExtractField;
