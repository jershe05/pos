<?php

namespace App\Contracts\Bl;

Interface FormatTextContract
{
    public function formatText(string $sText): string;
}
