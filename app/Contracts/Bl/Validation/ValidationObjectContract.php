<?php
namespace App\Contracts\Bl\Validation;

use Illuminate\Support\Str;

Interface ValidationObjectContract
{
    public function formatText();
}

