interface ConfirmPopUpContract
{
    showConfirmPopUp() : Promise<boolean>
}
export default ConfirmPopUpContract
