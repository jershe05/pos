<?php

namespace App\Providers;

use App\Bl\Registration\CategoryRegistration;
use App\Bl\Registration\UserRegistration;
use App\Contracts\Model\DatabaseContract;
use App\Model\Category;
use App\Model\User;
use Illuminate\Support\ServiceProvider;
use App\Constants\AppStrings;

class DatabaseServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserRegistration::class)
            ->needs(DatabaseContract::class)
            ->give(User::class);

        $this->app->when(CategoryRegistration::class)
            ->needs(DatabaseContract::class)
            ->give(Category::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
