import email from "../../Libraries/FormValidation/formObjects/email";
import inputText from "../../Libraries/FormValidation/formObjects/inputText";
import password from "../../Libraries/FormValidation/formObjects/password";
import formValidation from "../../Libraries/FormValidation/validation";
import MESSAGE_STRINGS from "../../Constants/User/MessageStrings";
import USER_ELEMENTS from "../../Constants/User/UserElements";
import User from "../../Models/Account/User";
import HttpRequest from "../../HttpRequest/HttpRequest";
import UserFieldSStrings from "../../Constants/User/UserFieldsStrings";
import CreateUser from "../../Bl/Account/CreateUser";
import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";
import ChangeBorder from "../../GeneralDOMEvents/ChangeBorder";

$(function() {
    let registration =  {
        init: function() {
            this.setRegistrationFields();
            this.setEvents();
        },

        setRegistrationFields: function()
        {
            let oFname = new inputText(USER_ELEMENTS.FNAME);
            let oLname = new inputText(USER_ELEMENTS.LNAME);
            let oEmail = new email(USER_ELEMENTS.EMAIL);
            let oPassword = new password(USER_ELEMENTS.PASSWORD);
            let oRepeatPassword = new password(USER_ELEMENTS.CONFIRM_PASSWORD, oPassword);

            oFname.setErrorMessage(MESSAGE_STRINGS.ERROR_FIRST_NAME);
            oLname.setErrorMessage(MESSAGE_STRINGS.ERROR_LAST_NAME);
            oEmail.setErrorMessage(
                MESSAGE_STRINGS.ERROR_EMAIL_EMPTY,
                MESSAGE_STRINGS.ERROR_EMAIL_INVALID
            );
            oPassword.setErrorMessage(MESSAGE_STRINGS.ERROR_PASSWORD);
            oRepeatPassword.setErrorMessage(MESSAGE_STRINGS.ERROR_REPEAT_PASSWORD);

            this.aFormObjects = {
                [UserFieldSStrings.FNAME]            : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oFname
                },
                [UserFieldSStrings.LNAME]            : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oLname
                },
                [UserFieldSStrings.EMAIL]            : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oEmail
                },
                [UserFieldSStrings.PASSWORD]         : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oPassword
                },
                [UserFieldSStrings.CONFIRM_PASSWORD] : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oRepeatPassword
                }
            };
        },

        setEvents: function() {
            USER_ELEMENTS.SUBMIT_BTN.click(function() {
                registration.register();
            });
        },

        register: async function () {
            let oChangeBorderColor = new ChangeBorder(this.aFormObjects);
            oChangeBorderColor.resetBorderColor();
            let oRegisterUser = new CreateUser(
                new User(new HttpRequest(), this.aFormObjects),
                new formValidation(this.aFormObjects, oChangeBorderColor),
                USER_ELEMENTS
            );
            await oRegisterUser.register();
            registration.setRegistrationFields();
        },
    };

    registration.init();
});
