<?php

namespace App\Bl\Validation;

use App\Constants\AppStrings;
use App\Contracts\Bl\Validation\ValidationContract;
use Illuminate\Support\Arr;

class Validation implements ValidationContract
{
    private $oFields;
    private $aResult = [];
    private $bValid = true;
    private $aFieldsToBeMatched;
    public function registerFields(array $oFields) : ValidationContract
    {
        $this->oFields = $oFields;
        return $this;
    }

    public function loopThroughObjectsAndExecuteValidation(array $aFields, array $aFieldsToBeMatched = []) : ValidationContract
    {
        $this->aFieldsToBeMatched = $aFieldsToBeMatched;
        foreach($aFields as $sField => $mValue) {
            $this->getFieldsToBeComparedMatched($sField, $mValue);
            if (Arr::has($this->oFields, $sField)) {
                $aResult = $this->oFields[$sField]
                    ->validateField($mValue, $sField)
                    ->formatText()
                    ->getResultAndErrorMessages();
                $this->setValidationResult($aResult, $sField);
            }
        }

        $this->compareMatchingFields();
        return $this;
    }

    public function setValidationResult(array $aResult, string $sField)
    {
        $this->bValid = $aResult[AppStrings::IS_VALID] && $this->bValid;
        $this->aResult = Arr::add(
            $this->aResult, $aResult[$sField],
            $aResult[AppStrings::RESULT]
        );
    }

    public function getFieldsToBeComparedMatched($sField, $mValue)
    {
        foreach($this->aFieldsToBeMatched as $iKey => $aIndexes) {
            if (array_key_first($aIndexes) === $sField || array_key_last($aIndexes) === $sField) {
                $this->aFieldsToBeMatched[$iKey][$sField] = $mValue;
            }
        }
    }

    public function compareMatchingFields()
    {
        foreach ($this->aFieldsToBeMatched as $aItem) {
            if (strcmp(current($aItem), next($aItem))){
                $this->bValid = false;
                $this->aResult = Arr::add(
                    $this->aResult, array_key_last($aItem),
                    [AppStrings::MESSAGES => array_key_first($aItem) . ' '. AppStrings::DID_NOT_MATCH]
                );
            }
        }
    }

    public function filterResult() : ValidationContract
    {
        if ($this->bValid === false) {
            $this->removeArrayIfEmpty();
        }
        return $this;
    }

    public function removeArrayIfEmpty() : ValidationContract
    {
        foreach ($this->aResult as $aItem => $mValue) {
            if (!is_array($mValue)) {
                unset($this->aResult[$aItem]);
            }
        }
        return $this;
    }

    public function getResult(): array
    {
        return [
            AppStrings::B_RESULT   => $this->bValid,
            AppStrings::RESULT => $this->aResult
        ];
    }
}
