
<!-- modal enter transaction details -->
<div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">TRANSACTION</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="input-group border mb-1">
                    <div class="input-group-append p-3  ">
                        <i class="fas fa-fingerprint fa-2x" ></i>
                    </div>
                    <div class="p-3">Party Balloons</div>
                </div>

                <div class="input-group border mb-1">
                    <div class="input-group-append p-3">
                        <i class="fas fa-ruble-sign fa-2x" ></i>
                    </div>
                    <input type="text" class="border-0 w-90 ml-2 input-focus_ text-danger font-weight-bold" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" readonly value="20.00">
                </div>

                <div class="input-group border mb-2">
                    <div class="input-group-append ">
                        <button class="btn btn-danger p-3" type="button">
                            <i class="far fa-money-bill-alt"></i>
                        </button>
                    </div>
                    <input type="text" class="border-0 w-90 ml-2 input-focus_ " placeholder="Cash..." aria-label="Search" aria-describedby="basic-addon2">
                </div>

                <div class="input-group border mb-2 float-right w-50">
                    <div class="input-group-append p-3">
                        <i class="fas fa-coins fa-2x"></i>
                    </div>
                    <div>
                        <h1 class="text-info p-1 float-right">0.00</h1>
                    </div>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-unique btn-danger w-100">FINISH <i class="fas fa-check ml-1"></i></button>
            </div>
        </div>
    </div>
</div>
