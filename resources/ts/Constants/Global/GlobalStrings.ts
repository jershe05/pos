const GLOBAL_STRINGS = {
    RESULT       : 'result',
    B_RESULT     : 'bResult',
    CHILD_INDEX  : 'child',
    PARENT_INDEX : 'parent',
    ADMIN_URL    : '/admin'
};
export default GLOBAL_STRINGS;
