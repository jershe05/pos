import ChangeBorder from "../../GeneralDOMEvents/ChangeBorder";
class setErrorMessages
{
    constructor(aMessages, oFieldElements)
    {
        this.aMessages = aMessages;
        this.oFieldElements = oFieldElements;
    }
    setErrorMessages(sMessage, bResult)
    {
        if (bResult === true) {
            return sMessage;
        }
        sMessage = '';
        let oFieldElements = this.oFieldElements;
        $.each(this.aMessages, function(sKey, oObject) {
            let oErrorEvent = new ChangeBorder(oFieldElements[sKey.toUpperCase()]);
            oErrorEvent.setErrorBorderColor();
            $.each(oObject, function (sSubKey, oSubObject) {
                sMessage += oSubObject + '<br />';
            })
        });
        return sMessage;
    }
}
export default setErrorMessages;
