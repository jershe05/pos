import DOMStrings from "../Constants/DOM/DOMStrings";
import GLOBAL_STRINGS from "../Constants/Global/GlobalStrings";
class ChangeBorder
{
    constructor(oObjects, sIndexOfElementToChangeBorderColor = GLOBAL_STRINGS.CHILD_INDEX) {
        this.oObjects = oObjects;
        this.sIndexOfElementToChangeBorderColor = sIndexOfElementToChangeBorderColor;
    }
    resetBorderColor() {
        let oSelf = this;
        $.each(this.oObjects, function(iKey, oDOM){
            let oNewDOM = oSelf.selectObjectsBasedOnIndex(oDOM);
            oNewDOM.child.focus(function(){
                oNewDOM.parent.css({
                    'border-color': DOMStrings.DEFAULT_BORDER_COLOR
                });
            });
        });
    }

    setErrorBorderColor() {
        this.oObjects.css('border-color', DOMStrings.ERROR_BORDER_COLOR);
    }

    overrideObject(oObject)
    {
        this.oObjects = oObject;
    }

    selectObjectsBasedOnIndex(oDOM) {
        let oNewDOM = {
            parent : '',
            child  : '',
        };

        if (this.sIndexOfElementToChangeBorderColor === GLOBAL_STRINGS.PARENT_INDEX) {
            oNewDOM.child = oDOM[GLOBAL_STRINGS.CHILD_INDEX].getElement();
            oNewDOM.parent = oDOM[GLOBAL_STRINGS.PARENT_INDEX];
            return oNewDOM;
        }

        oNewDOM.child = oDOM[GLOBAL_STRINGS.CHILD_INDEX].getElement();
        oNewDOM.parent = oDOM[GLOBAL_STRINGS.CHILD_INDEX].getElement();

        return oNewDOM;
    }

}
export default ChangeBorder;
