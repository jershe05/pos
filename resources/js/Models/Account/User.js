import URL from "../../Constants/User/Url";
import Loading from "../../Libraries/Curtain/Loading";
import ExtractField from "../ExtractField";

class User
{
    constructor(oHttpRequest, oFields)
    {
        this.oRegistration = oHttpRequest;
        this.oFields = oFields;
    }

   async save()
    {
        let oExtractField = new ExtractField(this.oFields);
        let oFields = oExtractField.exec();
        let LoadingPopUp = new Loading('Saving...', 'foldingCube');
        return await this.oRegistration.execute(oFields, URL.SAVE_REGISTRATION, LoadingPopUp);
    }

    async delete()
    {

    }

    async update()
    {

    }

    async uploadPicture()
    {

    }


}
export default User;
