<div class="col-xl-5 col-lg-5">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Shopping Cart <i class="fas fa-shopping-cart"></i></h6>
        </div>
        <div class="card-body" style="overflow-y:scroll; height: 400px">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Items</th>
                        <th>Qty</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>2</td>
                        <td>2.00</td>
                    </tr>
                    <tr>
                        <td>Garrett Winters</td>
                        <td>1</td>
                        <td>3.00</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="card-header py-3">
            <div class="float-right">
                <h1 class="text-danger">P 20.00</h1>
            </div>
            <div class=" float-right">
                <h2 class="mr-3 font-weight-bold text-primary">TOTAL:</h2>
            </div>
        </div>
        <div class="col-lg-12 mt-1 mb-1">
            <a href="#" class="btn btn-danger pt-3 pb-3 w-100">
                <h1>PAY NOW</h1>
            </a>
        </div>

        <div class="w-100 mt-1 mb-1 d-inline m-l-1">
            <a href="#" class="btn btn-info p-3 w-24">
                <i class="fa fa-print fa-2x"></i><br />
                <span>Print</span>
            </a>

            <a href="#" class="btn btn-success p-3 w-24">
                <i class="fa fa-percentage fa-2x"></i><br />
                <span>Void</span>
            </a>
            <a href="#" class="btn btn-primary p-3 w-24">
                <i class="fa fa-door-open fa-2x"></i><br />
                <span>Void</span>
            </a>
            <a href="#" class="btn btn-secondary p-3 w-24">
                <i class="fa fa-trash fa-2x"></i><br />
                <span>Void</span>
            </a>
        </div>
    </div>
</div>
