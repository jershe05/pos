import inputText from "../../Libraries/FormValidation/formObjects/inputText";
import formValidation from "../../Libraries/FormValidation/validation";
import MESSAGE_STRINGS from "../../Constants/Category/MessageStrings";
import CATEGORY_OBJECTS from "../../Constants/Category/CategoryObjects";
import HttpRequest from "../../HttpRequest/HttpRequest";
import CategoryFieldSStrings from "../../Constants/Category/CategoryFieldsStrings";
import AddCategory from "../../Bl/Category/AddCategory";
import Category from "../../Models/Category/Category";
import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";
import ChangeBorder from "../../GeneralDOMEvents/ChangeBorder";

$(function() {
    let category =  {
        init: function() {
            this.setCategoryFields();
            this.setEvents();
        },

        setCategoryFields: function()
        {
            let oName = new inputText(CATEGORY_OBJECTS.NAME);
            oName.setErrorMessage(MESSAGE_STRINGS.ERROR_NAME);
            let oDescription = new inputText(CATEGORY_OBJECTS.DESCRIPTION);
            oDescription.setOptional();
            this.aFormObjects = {
                [CategoryFieldSStrings.NAME]: {
                    [GLOBAL_STRINGS.CHILD_INDEX]  : oName,
                    [GLOBAL_STRINGS.PARENT_INDEX] : oName.getElement().parent()
                },
                [CategoryFieldSStrings.DECRIPTION] : {
                    [GLOBAL_STRINGS.CHILD_INDEX]  : oDescription,
                    [GLOBAL_STRINGS.PARENT_INDEX] : oDescription.getElement().parent()
                }
            };
        },

        setEvents: function() {
            CATEGORY_OBJECTS.SAVE_BTN.click(function() {
                category.add();
            });
        },

        add: async function () {
            let oChangeBorderColor = new ChangeBorder(this.aFormObjects, GLOBAL_STRINGS.PARENT_INDEX);
            oChangeBorderColor.resetBorderColor();
            let oAddCategory = new AddCategory(
                new Category(new HttpRequest(), this.aFormObjects),
                new formValidation(this.aFormObjects, oChangeBorderColor, GLOBAL_STRINGS.PARENT_INDEX),
                CATEGORY_OBJECTS
            );
            await oAddCategory.add();
            category.setCategoryFields();
        },
    };

    category.init();
});
