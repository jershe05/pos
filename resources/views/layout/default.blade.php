<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title')</title>
    <!-- Fonts, Css -->
    <link href="{{ asset('css/all.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/loadingModal.css') }}" rel="stylesheet" type="text/css">
    @stack('styles')
</head>
<body >
<!-- Page Wrapper -->
<div id="wrapper">
    @yield('sidebar-content')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
            @yield('content')
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Jothred's Store 2020</span>
                    </div>
                </div>
            </footer>
        </div>
</div>

</body>
<footer>
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('js/jquery-easing.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/loadingModal.js') }}"></script>
    @stack('scripts')
</footer>
</html>
