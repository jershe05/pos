import email from "../../Libraries/FormValidation/formObjects/email";
import password from "../../Libraries/FormValidation/formObjects/password";
import formValidation from "../../Libraries/FormValidation/validation";
import MESSAGE_STRINGS from "../../Constants/User/MessageStrings";
import USER_ELEMENTS from "../../Constants/User/UserElements";
import BTN from "../../Constants/FormObjects";
import HttpRequest from "../../HttpRequest/HttpRequest";
import UserFieldSStrings from "../../Constants/User/UserFieldsStrings";
import BlLogin from "../../Bl/Account/BlLogin";
import Login from "../../Models/Account/Login";
import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";
import ChangeBorder from "../../GeneralDOMEvents/ChangeBorder";

$(function() {
    let login =  {
        init: function() {
            this.setLoginFields();
            this.setEvents();
        },

        setLoginFields: function()
        {
            let oEmail = new email(USER_ELEMENTS.EMAIL);
            let oPassword = new password(USER_ELEMENTS.PASSWORD);
            oEmail.setErrorMessage(
                MESSAGE_STRINGS.ERROR_EMAIL_EMPTY,
                MESSAGE_STRINGS.ERROR_EMAIL_INVALID
            );
            oPassword.setErrorMessage(MESSAGE_STRINGS.ERROR_PASSWORD);

            this.aFormObjects= {
                [UserFieldSStrings.EMAIL]    : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oEmail
                },
                [UserFieldSStrings.PASSWORD] : {
                    [GLOBAL_STRINGS.CHILD_INDEX] : oPassword
                }
            }
        },

        setEvents: function() {
            BTN.SUBMIT_BTN.click(function() {
                login.execute();
            });
        },

        execute: async function () {
            let oChangeBorderColor = new ChangeBorder(this.aFormObjects);
            oChangeBorderColor.resetBorderColor();
            let oLogin = new BlLogin(
                new Login(new HttpRequest(), this.aFormObjects),
                new formValidation(this.aFormObjects, oChangeBorderColor),
                USER_ELEMENTS
            );
            await oLogin.login();
            login.setLoginFields();
        },
    };

    login.init()
});

