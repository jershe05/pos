import Swal from "sweetalert2";
import ConfirmPopUpContract from "./ConfirmPopUpContract";
class ConfirmPopUp implements ConfirmPopUpContract
{
    private sMessage  : string;
    private sBtnLabel : string;
    constructor(sMessage, sBtnLabel)
    {
        this.sMessage = sMessage;
        this.sBtnLabel = sBtnLabel;
    }

    showConfirmPopUp() : Promise <boolean>
    {
        return Swal.fire({
            title: 'Are you sure?',
            text: this.sMessage,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: this.sBtnLabel
        }).then((result) => {
            return result.value;
        })
    }
}
export default ConfirmPopUp;
