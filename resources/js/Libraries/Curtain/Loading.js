class Loading
{
    constructor(sMessage, sAnimation)
    {
        this.sMessage = sMessage;
        this.sAnimation = sAnimation;
    }

    show()
    {
        $('body').loadingModal({
            position:'auto',
            text: this.sMessage,
            color:'#fff',
            opacity:'0.7',
            backgroundColor:'red',
            animation: this.sAnimation
        });
    }
    hide()
    {
        $('body').loadingModal('hide');
    }

    destroy()
    {
        $('body').loadingModal('destroy');
    }

}
export default Loading;
