const MESSAGE_STRINGS = {
    ERROR_NAME       : 'Please fill-out the Category Name field',
    ADD_SUCCESS      : 'Successfully Add New Category!',
    CONFIRM_CONTINUE : 'Are you sure you want to add this category?',
    YES_LABEL        : 'Yes'
};

export default MESSAGE_STRINGS;
