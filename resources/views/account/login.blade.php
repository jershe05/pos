@extends('layout.default')
@section('title')
    Login
@endsection
@section('center')
    align-content-center
    @endsection
@section('content')
    @component('account.forms.login-form')
    @endcomponent
@endsection
@push('scripts')
    <script src="{{ asset('js/LoginController.js') }}"></script>
@endpush

