import GLOBAL_STRINGS from "../../Constants/Global/GlobalStrings";
class validation {
    constructor(aFormObjects, oChangeBorderColor, sIndexOfElementToChangeBorderColor = GLOBAL_STRINGS.CHILD_INDEX) {
        this.aFormObjects = aFormObjects;
        this.sIndexOfElementToChangeBorderColor = sIndexOfElementToChangeBorderColor;
        this.oChangeBorderColor = oChangeBorderColor;
    }

    checkErrors() {
        let bValid = true;
        let sMessage = '';
        let sIndexOfElementToChangeBorderColor = this.sIndexOfElementToChangeBorderColor;
        let oChangeBorderColor = this.oChangeBorderColor;
        $.each(this.aFormObjects, function(iKey, oObject) {
            if (oObject[GLOBAL_STRINGS.CHILD_INDEX].IfValid() === false) {
                bValid = false;
                sMessage += oObject[GLOBAL_STRINGS.CHILD_INDEX].getErrorMessage() + '<br />';

                let oObjectToChangeBorderColor = (sIndexOfElementToChangeBorderColor === GLOBAL_STRINGS.CHILD_INDEX) ?
                    oObject[sIndexOfElementToChangeBorderColor].getElement() : oObject[sIndexOfElementToChangeBorderColor];
                oChangeBorderColor.overrideObject(oObjectToChangeBorderColor);
                oChangeBorderColor.setErrorBorderColor();
            }

        });

        return {
            'bResult' : bValid,
            'sMessage' : sMessage
        }
    }

}

export default validation;
