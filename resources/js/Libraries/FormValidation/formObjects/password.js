class inputText
{
    constructor(oInputText) {
        this.oInputText = oInputText;
    }

    setErrorMessage(sErrorMessage) {
        this.sErrorMessage = sErrorMessage;
        return this;
    }

    getErrorMessage() {
        return this.sErrorMessage;
    }

    getElement() {
        return this.oInputText;
    }

    IfValid() {
        this.oInputText.val($.trim(this.oInputText.val()));
        return this.oInputText.val().length > 7;
    }

    getElement() {
        return this.oInputText;
    }

}

export default inputText;
