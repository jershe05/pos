import ObjectValidationContract from "../FormValidationContracts/ObjectValidationContract";

class TextValidation implements ObjectValidationContract
{
    private oObject : any;
    private bResult : boolean = false;

    sanitize(oObject: any) : ObjectValidationContract {
        this.oObject.val((oObject.val().trim()));
        return this;
    }

    checkRule() : ObjectValidationContract
    {
        this.bResult = this.oObject.val().length > 1;
        return this;
    }

    getResult(): boolean {
        return this.bResult;
    }
}

export default TextValidation;


