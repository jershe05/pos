import ObjectValidationContract from "../FormValidationContracts/ObjectValidationContract";

class EmailValidation implements ObjectValidationContract
{
    private oObject : any;
    private bResult : boolean = false;

    sanitize(oObject: any) : ObjectValidationContract {
        this.oObject.val((oObject.val().trim()));
        return this;
    }

    checkRule() : ObjectValidationContract
    {
        const sValidator = /\S+@\S+\.\S+/;
        this.bResult = sValidator.test(String(this.oObject.val()).toLowerCase());
        return this;
    }

    getResult(): boolean {
        return this.bResult;
    }
}

export default EmailValidation;
