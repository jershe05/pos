<?php

namespace App\Controllers\Registration;

use App\Bl\Validation\ValidationObjects\DefaultField;
use App\Bl\Validation\ValidationObjects\PasswordFormatField;
use App\Bl\Validation\ValidationObjects\TitleFormatField;
use App\Constants\AppStrings;
use App\Contracts\Bl\RegistrationContract;
use App\Contracts\Bl\Validation\ValidationContract;
use App\Contracts\Controllers\RegisterControllerContract;
use Illuminate\Http\Request;

class UserRegistrationController  extends RegistrationControllerAbstract implements RegisterControllerContract
{
    public function __construct(RegistrationContract $oBlRegistration, ValidationContract $oBlValidation)
    {
        parent::__construct($oBlRegistration, $oBlValidation);
    }

    public function instantiateObjectFields(): array
    {
        return [
            AppStrings::EMAIL_FIELD => new DefaultField(AppStrings::EMAIL_RULES),
            AppStrings::FNAME_FIELD => new TitleFormatField(AppStrings::FNAME_RULES),
            AppStrings::LNAME_FIELD => new TitleFormatField(AppStrings::LNAME_RULES),
            AppStrings::PASSWORD_FIELD => new PasswordFormatField(AppStrings::PASSWORD_RULES)
        ];
    }

    public function store(Request $aUserDetails) : array
    {
        $aUserDetails = $aUserDetails->all()[AppStrings::PARAMS];
        $oFields = $this->instantiateObjectFields();
        $aFieldsToMatch = $this->setFieldsToMatch();
        return $this->save($aUserDetails, $oFields, $aFieldsToMatch);
    }

    public function setFieldsToMatch() : array
    {
        return [
            [
                AppStrings::PASSWORD_FIELD => '',
                AppStrings::CONFIRM_PASSWORD_FIELD => ''
            ]
        ];
    }
}
