<?php

use App\Constants\AppStrings;

Route::post(AppStrings::SAVE_CATEGORY_ROUTE, 'CategoryRegistrationController@store')->middleware('adminAccess');
