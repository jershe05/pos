<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Constants\AppStrings;

Route::get(AppStrings::LOGIN_ROUTE, function () {
    return view('account.login');
})->middleware('guest');

Route::get(AppStrings::LOG_OUT_ROUTE, function () {
    return redirect(AppStrings::LOGIN_ROUTE);
})->middleware('logout');

Route::get(AppStrings::FORGOT_PASSWORD, function () {
    return view('account.forgot-password');
});

Route::post(AppStrings::AUTHENTICATE_ROUTE, 'LoginController@authenticate');

Route::get( AppStrings::ACCOUNT_ROUTES[0], function (){
    return view('admin.index');
})->middleware('adminAccess');

Route::get( AppStrings::ACCOUNT_ROUTES[1], function (){
    return view('pos.index');
})->middleware('posAccess');
