<?php

namespace App\Providers;
use App\Bl\Validation\Validation;
use App\Contracts\Bl\Validation\ValidationContract;
use App\Contracts\Model\AccountContract;
use App\Controllers\Auth\LoginController;
use App\Controllers\Category\CategoryRegistrationController;
use App\Controllers\Registration\UserRegistrationController;
use App\Http\Middleware\AdminMiddleware;
use App\Http\Middleware\RedirectIfAuthenticated;
use App\Model\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->when(UserRegistrationController::class)
            ->needs(ValidationContract::class)
            ->give(Validation::class);

        $this->app->when(CategoryRegistrationController::class)
            ->needs(ValidationContract::class)
            ->give(Validation::class);

        $this->app->when(RedirectIfAuthenticated::class)
            ->needs(AccountContract::class)
            ->give(User::class);

        $this->app->when(AdminMiddleware::class)
            ->needs(AccountContract::class)
            ->give(User::class);

        $this->app->when(LoginController::class)
            ->needs(ValidationContract::class)
            ->give(Validation::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
