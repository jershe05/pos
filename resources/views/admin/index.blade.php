@extends('layout.default')
@section('title')
    Point of Sale
@endsection

@section('sidebar-content')
    @component('layout.side-panel')
        @endcomponent
@endsection

@section('content')
    @component('layout.nav')
    @endcomponent
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <div class="row">
                  @component('product.section')
                      @endcomponent

                  @component('product.sales')
                  @endcomponent
                </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->
@endsection


<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

@component('product.add')
    @endcomponent

@component('stocks.add')
@endcomponent

@component('layout.logout')
@endcomponent

@component('product.details')
@endcomponent

@component('pos.order-details')
@endcomponent

@component('category.add')
@endcomponent

@component('category.list')
@endcomponent

@push('scripts')
    <script src="{{ asset('js/admin.js') }}"></script>
@endpush
