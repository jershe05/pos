const UserFieldSStrings = {
    FNAME            : 'fname',
    LNAME            : 'lname',
    EMAIL            : 'email',
    PASSWORD         : 'password',
    CONFIRM_PASSWORD : 'confirm_password',
};

export default UserFieldSStrings;
