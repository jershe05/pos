@extends('layout.default')
@section('title')
    Registration
@endsection
@section('center')
    align-content-center
    @endsection
@section('content')
    @component('account.forms.registration')
    @endcomponent
@endsection
@push('scripts')
    <script src="{{ asset('js/UserRegistrationController.js') }}"></script>
@endpush

