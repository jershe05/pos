const CATEGORY_OBJECTS = {
    NAME        : $('#category_name'),
    DESCRIPTION : $('#category_description'),
    SAVE_BTN    : $('#category_add_btn')
};

export default CATEGORY_OBJECTS;
