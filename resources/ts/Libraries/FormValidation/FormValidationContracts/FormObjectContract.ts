interface FormObjectContract
{
    setOptional()               : FormObjectContract;
    setErrorMessage(
        sEmptyErrorMessage   : string,
        sInvalidErrorMessage : string
    )                           : FormObjectContract;
    setMessageAndCheckIfEmpty() : boolean;
    getErrorMessage()           : string;
    getElement()                : any;
    IfValid()                   : boolean;
}
export default FormObjectContract;
